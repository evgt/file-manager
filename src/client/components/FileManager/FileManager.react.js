import PropTypes from 'prop-types';
import React, { Component, Children } from 'react';
import './FileManager.less';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContextProvider } from 'react-dnd';

const propTypes = {
  className: PropTypes.string
};
const defaultProps = {};

export default
class FileManager extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  static contextTypes = {
    dragDropManager: PropTypes.object.isRequired
  };

  static childContextTypes = {
    dragDropManager: PropTypes.object.isRequired
  };

  wrapByProvider(child){
    if(!!this.context.dragDropManager){
      return child;
    }

    return <DragDropContextProvider backend={HTML5Backend}>
      {child}
    </DragDropContextProvider>
  }

  render() {
    let { children, className } = this.props;

    return (
      <div className={`oc-fm--file-manager ${className || ''}`}>
        {this.wrapByProvider(
          <div className="oc-fm--file-manager__navigators">
            {Children.toArray(children).map((child, i) => (
              <div key={i} className="oc-fm--file-manager__navigator">
                {child}
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}

FileManager.propTypes = propTypes;
FileManager.defaultProps = defaultProps;
